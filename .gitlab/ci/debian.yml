# ---------------------------
# Debian packaging workflow
# ---------------------------

include:
  # https://computing.docs.ligo.org/gitlab-ci-templates/
  - project: computing/gitlab-ci-templates
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/
    file: debian.yml
  # local test template
  - local: /.gitlab/ci/test.yml

# -- macros

.buster:
  image: igwn/builder:buster

.bullseye:
  image: igwn/builder:bullseye

# -- source packages --------
#
# These jobs make DSC packages
#

.dsc:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:dsc
    - .debian:dsc
  stage: source packages
  needs:
    - tarball
  variables:
    TARBALL: "igwn-htcondor-config-*.tar.*"
  before_script:
    - !reference [".debian:dsc", "before_script"]
    # unpack the tarball, update the debian changelog, and pack it up again
    - apt-get -yqq install devscripts
    - export DEBFULLNAME="GitLab"
    - export DEBEMAIL="gitlab@git.ligo.org"
    - tar -zxf ${TARBALL}
    - (cd igwn-htcondor-config-*/ && dch --rebuild 'ci')
    - tar -zcf ${TARBALL} igwn-htcondor-config-*/

dsc:buster:
  extends:
    - .dsc
    - .buster

dsc:bullseye:
  extends:
    - .dsc
    - .bullseye

# -- binary packages --------
#
# These jobs generate DEB
# binary packages from the
# DSC sources packages
#

.deb:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:deb
    - .debian:deb
  stage: binary packages
  variables:
    DSC: "igwn-htcondor-config_*.dsc"

deb:buster:
  extends:
    - .deb
    - .buster
  needs:
    - dsc:buster

deb:bullseye:
  extends:
    - .deb
    - .bullseye
  needs:
    - dsc:bullseye

# -- test -------------------

.test:debian:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:base
    - .debian:base
    - .test
  before_script:
    # set up apt
    - !reference [".debian:base", before_script]
    # setup local apt repository
    - apt-get -y -q -q install local-apt-repository
    - mkdir -pv /srv/local-apt-repository
    # fill our local apt repo and rebuild it
    - mv -v *.deb /srv/local-apt-repository
    - /usr/lib/local-apt-repository/rebuild
    - apt-get -y -q update
    # show our package(s)
    - apt-cache policy igwn-htcondor-config*
    # install htcondor and the igwn-htcondor-config
    - apt-get -y install htcondor igwn-htcondor-config

test:buster:
  extends:
    - .test:debian
    - .buster
  needs:
    - deb:buster
  image: igwn/base:buster

test:bullseye:
  extends:
    - .test:debian
    - .bullseye
  needs:
    - deb:bullseye
  image: igwn/base:bullseye

# -- lint -------------------
#
# These jobs check the code
# for quality issues
#

.lintian:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:lint
    - .debian:lint
  stage: code quality
  variables:
    LINTIAN_OPTIONS: "--color always --suppress-tags new-package-should-close-itp-bug --fail-on warning --allow-root --pedantic"

lintian:buster:
  extends:
    - .lintian
    - .buster
  needs:
    - deb:buster
  variables:
    LINTIAN_OPTIONS: "--color always --suppress-tags new-package-should-close-itp-bug --fail-on-warnings --allow-root --pedantic"

lintian:bullseye:
  extends:
    - .lintian
    - .bullseye
  needs:
    - deb:bullseye
