%define srcname igwn-htcondor-config
%define version 20220414
%define release 1

Name: %{srcname}
Version: %{version}
Release: %{release}%{?dist}
Summary: HTCondor configuration for IGWN
License: Apache-2.0
Url: https://git.ligo.org/computing/igwn-htcondor-config
Source0: https://software.igwn.org/sources/source/%{srcname}-%{version}.tar.gz
Packager: Duncan Macleod <duncan.macleod@ligo.org>
Prefix: %{_prefix}

# build
BuildArch: noarch

# -- packages ---------------

# -- metapackage

Requires: %{name}-accounting = %{version}-%{release}

%description
IGWN-specific configuration files for HTCondor

%files
%doc README.md
%license LICENSE

# -- accounting

%package accounting
Summary: %{summary} Accounting

%description accounting
IGWN Usage Accounting configuration for HTCondor

%files accounting
%doc README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/condor/config.d/90-igwn-accounting.conf

# -- build ------------------

%prep
%autosetup -n %{srcname}-%{version}

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/condor/config.d/
%__install -m 644 -p -v config/*.conf %{buildroot}%{_sysconfdir}/condor/config.d/

# -- changelog --------------

%changelog
* Thu Apr 14 2022 Duncan Macleod <duncan.macleod@ligo.org> - 0.0.1-1
- First release with accounting configuration
