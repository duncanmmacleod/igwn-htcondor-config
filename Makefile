VERSION = $(shell grep "%define version" *.spec | cut -d\  -f3)

.PHONY: dist
dist: igwn-htcondor-config-${VERSION}.tar.gz

igwn-htcondor-config-%.tar.gz:
	git archive \
		--format=tar.gz \
		--output=$@ \
		--prefix=igwn-htcondor-config-$*/ \
		HEAD

.PHONY: clean
clean:
	rm -f igwn-htcondor-config-*.tar.*
